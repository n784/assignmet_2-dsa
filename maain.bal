import ballerinax/kafka;
import ballerina/log;
import ballerinax/mongodb;
import ballerina/io;

kafka:ConsumerConfiguration consumerConfigs = {
    groupId: "group-shipping",
    
    topics: ["processedOrder"],

    pollingInterval: 1
};

public type Shipping record {|
    string city;
    string street;
    int erf;
|};

public type Item record {
    string sku;
    int quantity;
    int store = 0;
};

public type Order record {|
    int userId = 0;
    int orderId;
    boolean isValid;
    Shipping shipTo;
    Item[] items;
|};

public type ShippingOrder record {
    record {
        int store;
        int quantity;
    }[] collection = [];
    int total = 0;
    string sku = "";
};

public type SuccessfulOrder record {|
    int userId = 0;
    int orderId;
    boolean isValid;
    Shipping shipTo;
    ShippingOrder[] items;
|};


public type OrderConsumerRecord record {|
    *kafka:AnydataConsumerRecord;
    SuccessfulOrder value;
|};

public type OrderProducerRecord record {|
    *kafka:AnydataProducerRecord;
    Order value;
|};

mongodb:ConnectionConfig mongoConfig = {
    options: {url: "mongodb+srv://dbUser:dbUserPassword@cluster0"}
};
string database = "order";
mongodb:Client mongoClient = check new (mongoConfig, database);

kafka:Producer producer = check new (kafka:DEFAULT_URL);

service on new kafka:Listener(kafka:DEFAULT_URL, consumerConfigs) {
    remote function onConsumerRecord(OrderConsumerRecord[] records) returns error? {
        io:println("###############((((((((((((((((((((((((((((())))))))))))))))");
        
        check from OrderConsumerRecord orderRecord in records
            do {
                string|mongodb:Error? res = check mongoClient->insert({
                    "order_id": orderRecord.value.orderId,
                    "status": "pending",
                    "city": orderRecord.value.shipTo.city,
                    "street": orderRecord.value.shipTo.street,
                    "erf": orderRecord.value.shipTo.erf,
                    "details": orderRecord.value.items.toJson()
                    }
                    , "shipping");

                if (res is string) {
                    log:printInfo("Created shipping record");
                    log:printInfo(res);
                } else {
                    log:printInfo("Could not add shipping record");
                    io:println(res);
                }

            };

    }
}
